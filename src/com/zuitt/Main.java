package com.zuitt;

public class Main {

    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        Contact firstContact = new Contact();
        Contact secondContact = new Contact();

        firstContact.setName("John Doe");
        firstContact.setNumbers("+639152468596");
        firstContact.setNumbers("+639228547963");
        firstContact.setAddresses("Quezon City");
        firstContact.setAddresses("Makati City");

        secondContact.setName("Jane Doe");
        secondContact.setNumbers("+639162148573");
        secondContact.setNumbers("+639173698541");
        secondContact.setAddresses("Caloocan City");
        secondContact.setAddresses("Pasay City");

        phonebook.setContacts(firstContact);
        phonebook.setContacts(secondContact);

        if (phonebook.getContacts().isEmpty()) {
            System.out.println("The phonebook is empty");
        } else {
//            System.out.println("Name: " + firstContact.getName());
//            System.out.println("Number/s: " + firstContact.getNumbers());
//            System.out.println("Addresses: " + firstContact.getAddresses());
//            System.out.println();
//            System.out.println("Name: " + secondContact.getName());
//            System.out.println("Number/s: " + secondContact.getNumbers());
//            System.out.println("Addresses: " + secondContact.getAddresses());

            for (var i = 0; i < phonebook.getContacts().toArray().length; i++) {
                System.out.println(phonebook.getContacts().get(i));
            }
        }
    }
}
