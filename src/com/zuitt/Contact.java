package com.zuitt;

import java.util.ArrayList;

public class Contact {
    private String name;
    private ArrayList<String> numbers = new ArrayList<String>();
    private ArrayList<String> addresses = new ArrayList<String>();

    public Contact () {};

    public Contact (String name, ArrayList<String> number, ArrayList<String> address) {
        this.name = name;
        this.numbers = number;
        this.addresses = address;
    }

    public String getName() {return name;}
    public ArrayList<String> getNumbers() {return numbers;}
    public ArrayList<String> getAddresses() {return addresses;}

    public void setName(String name) {this.name = name;}
    public void setNumbers(String number) {this.numbers.add(number);}
    public void setAddresses(String address) {this.addresses.add(address);}

    @Override
    public String toString() {
        return "Name: " + name + "\nNumber/s: " + numbers + "\nAddresses: " + addresses + "\n";
    }
}