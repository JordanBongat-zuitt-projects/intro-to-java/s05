package com.zuitt;

import java.util.ArrayList;

public class Phonebook {
    private ArrayList<Object> contacts = new ArrayList<Object>();

    public Phonebook () {};

    public Phonebook (ArrayList<Object> contact) {
        this.contacts = contact;
    }

    public ArrayList<Object> getContacts () {return contacts;}
    public void setContacts (Object contact) {this.contacts.add(contact);}
}